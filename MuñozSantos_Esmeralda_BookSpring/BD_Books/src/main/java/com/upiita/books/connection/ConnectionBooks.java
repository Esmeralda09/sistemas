package com.upiita.books.connection;

import java.sql.*;
import java.util.ArrayList;

import com.upiita.books.model.Book;

public class ConnectionBooks {
	 static Connection conn = null;
     static String dbURL ="jdbc:mysql://localhost:3306/bookStore";
     static String user = "root";
     static String pass = "Mania123$";
     
   public ConnectionBooks() {
	   try {
		conn = DriverManager.getConnection(dbURL, user, pass);
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
   }
   
   public ArrayList<Book> consultar() {
	   ResultSet resultSet = null;
	   ArrayList<Book> listBook = new ArrayList<>();
	   Book b = new Book();
	   try {
		   PreparedStatement ps = conn.prepareStatement("SELECT * FROM books");
		   resultSet = ps.executeQuery();
		   while (resultSet.next()) {
			   b.setId(resultSet.getString("id"));
			   b.setName(resultSet.getString("name"));
			   b.setDescription(resultSet.getString("description"));
			   b.setPrice(resultSet.getFloat("price"));
			   b.setEliminado(resultSet.getInt("eliminado"));
			   listBook.add(b);
		   }
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
	   return listBook;
   }
   
   public void crear(Book b) {
	   String sql = "INSERT INTO books VALUES('" + b.getId() + "','" + b.getName()
	   		+ "','" +b.getDescription() + "',"+b.getPrice()+ "," + b.getEliminado() + ")";
	   try {
		   PreparedStatement ps = conn.prepareStatement(sql);
		   ps.executeUpdate(sql);
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
   }
   
   public void actualizar(Book b) {
	   String sql = "UPDATE books SET  name='" + b.getName()+ "' description='" + 
   b.getDescription() + "' price="+b.getPrice()+ " eliminado=" + b.getEliminado() + " WHERE id='" + b.getId() + "'";
	   try {
		   PreparedStatement ps = conn.prepareStatement(sql);
		   ps.executeUpdate(sql);
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
   }
   
   public void eliminar(Book b) {
	   String sql = "UPDATE books SET eliminado=1 WHERE id='" + b.getId() + "'";
	   try {
		   PreparedStatement ps = conn.prepareStatement(sql);
		   ps.executeUpdate(sql);
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
   }
}
