	package com.upiita.books.controller;


import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.upiita.books.connection.*;
import com.upiita.books.model.Book;

@RestController
@RequestMapping("/books")
public class BdBooksController {
	@GetMapping
	public ArrayList<Book> getBooks() {
		ConnectionBooks Bd = new ConnectionBooks();
		return Bd.consultar();
	}
	
	@PostMapping(consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String>createBook(@RequestBody  Book book){
		ConnectionBooks Bd = new ConnectionBooks();
		Bd.crear(book);
		return new ResponseEntity<String>("Book created",HttpStatus.OK);
	}
	@PutMapping(consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String>UpdateBook(@RequestBody  Book book){
		ConnectionBooks Bd = new ConnectionBooks();
		Bd.actualizar(book);
		return new ResponseEntity<String>("Book updated",HttpStatus.OK);
	}
	
	@DeleteMapping(consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String>DeleteBook(@RequestBody  Book book){
		ConnectionBooks Bd = new ConnectionBooks();
		Bd.eliminar(book);
		return new ResponseEntity<String>("Book delected",HttpStatus.OK);
	}
}
